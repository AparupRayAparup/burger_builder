import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { connect } from 'react-redux';

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary';
import ContactData from './ContactData/ContactData';

class Checkout extends Component {
    // state = {
    //     ingredients: null,
    //     price: 0
    // }

    // componentWillMount () {
    //     const query = new URLSearchParams( this.props.location.search );
    //     const ingredients = {};
    //     let price = 0;
    //     for ( let param of query.entries() ) {
    //         // ['salad', '1']
    //         if (param[0] === 'price') {
    //             price = param[1];
    //         } else {
    //             ingredients[param[0]] = +param[1];
    //         }
    //     }
    //     this.setState( { ingredients: ingredients, totalPrice: price } );
    // }

    checkoutCancelledHandler = () => {
        this.props.history.goBack();
    }

    checkoutContinuedHandler = () => {
        this.props.history.replace( '/checkout/contact-data' );
    }

    render () {

        let checkoutSummary = <Redirect to="/"/>; 
        
        if(this.props.ings) {
            const purchasedOrder = this.props.purchased ? <Redirect to="/"/> : null;
            checkoutSummary = (
                <div>
                    {purchasedOrder}
                    <CheckoutSummary
                        ingredients={this.props.ings}
                        checkoutCancelled={this.checkoutCancelledHandler}
                        checkoutContinued={this.checkoutContinuedHandler} />
                    <Route 
                    path={this.props.match.path + '/contact-data'} 
                    // render={(props) => (<ContactData ingredients={this.props.ings} price={this.props.priceTotal} {...props} />)} />
                    component={ContactData}/> 
                </div>       
            )
        }

        //To add the loader
        // if(!this.props.ings) {
        //     checkoutSummary = <Spinner/>;
        // }

        return checkoutSummary;
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        priceTotal: state.burgerBuilder.totalPrice,
        purchased: state.order.purchased
    }
}

// const mapDispatchToProps = dispatch => {
//     return {
//         onorderInit: () => dispatch(actionCreators.orderInit())
//     }
// }

export default connect(mapStateToProps)(Checkout);