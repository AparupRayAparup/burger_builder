import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers , applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga'; //redux saga is redux middleware library that is 
//is designed to make handling side effects in our redux app. It achieves this using generators.
//It allowing us to write asynchronous code that looks synchronous

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import burgerBuilderReducer from './store/reducers/burgerBuilder';
import orderIngredientsReducer from './store/reducers/order';
import authenticationReducer from './store/reducers/auth';
import { authWatch , watchBurgerBuilder, watchOrder } from './store/sagas';

const rootReducer = combineReducers({
    burgerBuilder: burgerBuilderReducer,
    order: orderIngredientsReducer,
    auth: authenticationReducer
})

const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;

// const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk, sagaMiddleware)));

sagaMiddleware.run(authWatch);
sagaMiddleware.run(watchBurgerBuilder);
sagaMiddleware.run(watchOrder);

//video 9 done upcomming 10

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render( app, document.getElementById( 'root' ) );
registerServiceWorker();
