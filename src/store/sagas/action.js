import { put, delay, call } from 'redux-saga/effects';
import axios from 'axios';

import * as actions from '../actions/index';

export function* logOutSaga(action) {
    // yield localStorage.removeItem('Token');
    // yield localStorage.removeItem('Expiration-time'); 
    // yield localStorage.removeItem('userId');
    
    //function* is actually kind of function that is generator where we can
    // pause the function execution for async task
    // yield keyword(will resume the generator function with resolved response) - it will wait to finish 
    // first task then it will execute second task means 
    // it will execute task asynchronously. It will pause the
    // function execution untill promise is resolve or rejected                                        


    //Another way we can use call function that makes our generator testable. we can mock this and not really execute this code
    //but previously we need to exceute always everything

    yield call([localStorage, 'removeItem'], "Token");
    yield call([localStorage, 'removeItem'], "Expiration-time");
    yield call([localStorage, 'removeItem'], "userId");

    yield put(actions.logoutSucceed());
}

export function* checkAuthTimeoutSaga(action) {
    yield delay(action.expirationTime * 1000);
    yield put(actions.logOut)
}

export function* authUserSaga(action) {
    yield put(actions.authStart())

        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAbGleNCftob0fTvjhIkx_mGL_DK4AT_EE';

        if(!action.isSignup) {
            url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAbGleNCftob0fTvjhIkx_mGL_DK4AT_EE'
        }
        const authData = {
            email: action.email,
            password: action.password,
            returnSecureToken: true
        }
        try {
            const response = yield axios.post(url, authData)
            console.log(response.data);
            const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
            localStorage.setItem('Token',response.data.idToken);
            localStorage.setItem('Expiration-time',expirationDate);
            localStorage.setItem('userId',response.data.localId);
            yield put(actions.authSuccess(response.data));
            yield put(actions.checkAuthTimeout(response.data.expiresIn));
        }
        catch(error) {
            yield put(actions.authFail(error.response.data.error));
        }
}

export function* authCheckStateSaga() {
    const token = yield localStorage.getItem('Token');

        if(!token) {
            yield put(actions.logOut());
        }
        else {
            const expirationDate = yield new Date(localStorage.getItem('Expiration-time'));
            //////left here
            if(expirationDate > new Date()) {
                const userId = yield localStorage.getItem('userId');

                const authData = {
                    idToken: token,
                    localId: userId
                }
                yield put(actions.authSuccess(authData));
                yield put(actions.checkAuthTimeout((expirationDate.getTime() - new Date().getTime())/1000));
            }
            else {
                yield put(actions.logOut());
            }
        }
}

