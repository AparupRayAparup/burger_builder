import { put } from 'redux-saga/effects';

import axios from '../../axios-orders';
import * as actions from '../actions/index';

export function* orderIngredientsSaga(action) {
        yield put(actions.orderingredientStart());
        const response = yield axios.post( '/orders.json?auth='+action.token, action.orderDetails )
            try {
                yield put(actions.orderingredientSuccess(response.data,action.orderDetails));
            }
            catch(error) {
                yield put(actions.orderingredientFailure(error));
            }
}

export function* fetchOrdersSaga(action) {
    yield put(actions.fetchorderStart());
    const res = yield axios.get('/orders.json?auth='+action.token)
    try {
        const fetchedOrders = [];

            const checkUserId = yield localStorage.getItem('userId');

            for (let key in res.data) {
                console.log(res.data[key].userId);
                if(res.data[key].userId === checkUserId) {
                    fetchedOrders.push({
                        ...res.data[key],
                        id: key
                    });
                }
            }
            console.log(fetchedOrders);
            // console.log('getState value =',getState().auth.token);
            //using getState function you can access the current value of redux store state
            //I am not using it here just passing the token as a query params
            yield put(actions.fetchorderSuccess(fetchedOrders));
    }
    catch(error) {
        yield put(actions.fetchorderFailure(error))
    }
}