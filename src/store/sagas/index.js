import { takeEvery, all, takeLatest} from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';
import { logOutSaga, checkAuthTimeoutSaga, authUserSaga, authCheckStateSaga } from '../sagas/action';
import { initIngredientsSaga } from '../sagas/burgerBuilder'
import { orderIngredientsSaga, fetchOrdersSaga } from './order';

export function* authWatch() {
    // yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logOutSaga);
    // yield takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga);
    // yield takeEvery(actionTypes.AUTH_USER, authUserSaga);
    // yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga);

    //another way instead of using multiple time yield keyword you can use yield all at a time for multiple actionTypes

    yield all([
        takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logOutSaga),
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
        takeEvery(actionTypes.AUTH_USER, authUserSaga),
        takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga)
    ])
}

export function* watchBurgerBuilder() {
    yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

export function* watchOrder() {
    // yield takeEvery(actionTypes.ORDER_INGREDIENTS, orderIngredientsSaga);
    yield takeLatest(actionTypes.ORDER_INGREDIENTS, orderIngredientsSaga); //takeLatest will cancel any ongoing function execution and always execute the latest one
    yield takeEvery(actionTypes.FETCH_ORDER, fetchOrdersSaga);
}