import * as actionTypes from './actionTypes';


export const orderingredientSuccess = (id,orderDetails) => {
    return {
        type: actionTypes.ORDER_INGREDIENTS_SUCCESS,
        orderId: id,
        orderDetails: orderDetails
    }
}

export const orderingredientFailure = (error) => {
    return {
        type: actionTypes.ORDER_INGREDIENTS_FAILURE,
        error: error
    }
}

export const orderingredientStart = () => {
    return {
        type: actionTypes.ORDER_INGREDIENTS_START
    }
}

export const orderIngredients = (orderDetails,token) => {
    // return dispatch => {
    //     dispatch(orderingredientStart());
    //     axios.post( '/orders.json?auth='+token, orderDetails )
    //         .then( response => {
    //             console.log(response.data);
    //             dispatch(orderingredientSuccess(response.data,orderDetails));
    //         } )
    //         .catch( error => {
    //             dispatch(orderingredientFailure(error));
    //         } );
    // }

    return {
        type: actionTypes.ORDER_INGREDIENTS,
        orderDetails: orderDetails,
        token: token
    }
}

export const orderInit = () => {
    return {
        type: actionTypes.ORDER_INIT
    }
}


//Orders page actions
export const fetchorderStart = () => {
    return {
        type: actionTypes.FETCH_ORDER_START
    }
}

export const fetchorderSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDER_SUCCESS,
        orders: orders
    }
}

export const fetchorderFailure = (error) => {
    return {
        type: actionTypes.FETCH_ORDER_FAILURE,
        error: error
    }
}

export const fetchOrders = (token) => {
    // return (dispatch,getState) => {
    //     dispatch(fetchorderStart());
    //     axios.get('/orders.json?auth='+token)
    //     .then(res => {
    //         const fetchedOrders = [];

    //         const checkUserId = localStorage.getItem('userId');

    //         for (let key in res.data) {
    //             console.log(res.data[key].userId);
    //             if(res.data[key].userId === checkUserId) {
    //                 fetchedOrders.push({
    //                     ...res.data[key],
    //                     id: key
    //                 });
    //             }
    //         }
    //         console.log(fetchedOrders);
    //         // console.log('getState value =',getState().auth.token);
    //         //using getState function you can access the current value of redux store state
    //         //I am not using it here just passing the token as a query params
    //         dispatch(fetchorderSuccess(fetchedOrders));
    //     })
    //     .catch(err => {
    //         dispatch(fetchorderFailure(err))
    //     });
    // }
    return {
        type: actionTypes.FETCH_ORDER,
        token: token
    }
}