import * as actionTypes from '../actions/actionTypes';

const initialState = {
    orderDetails: [],
    loading: false,
    purchased: false
}

const reducer = (state = initialState,action) => {
    switch(action.type) {
        case actionTypes.ORDER_INIT:
            return {
                ...state,
                purchased: false
            }
        case actionTypes.ORDER_INGREDIENTS_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.ORDER_INGREDIENTS_SUCCESS:

        const newOrder = {
            ...action.orderDetails,
            id: action.orderId
        }
            return {
                ...state,
                orderDetails: state.orderDetails.concat(newOrder),
                loading: false,
                purchased: true
            }
        case actionTypes.ORDER_INGREDIENTS_FAILURE:
            return {
                ...state,
                loading: false
            }
        case actionTypes.FETCH_ORDER_START:
            return {
                ...state,
                loading: true
            }
        case actionTypes.FETCH_ORDER_SUCCESS:
            return {
                ...state,
                loading: false,
                orderDetails: action.orders
            }
        case actionTypes.FETCH_ORDER_FAILURE:
             return {
                 ...state,
                 loading: false
             }    
        default: 
            return state;    
    }
}

export default reducer;