import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burgerbuilder-8402c.firebaseio.com/'
});

export default instance;